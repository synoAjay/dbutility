package com.db.dbUtility;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Map;
import java.util.Objects;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
@Component
@Qualifier("msSqlDatabaseUtility")
public class MsSqlDatabaseUtility implements DatabaseUtility {

	@Autowired
	AES aes;
	
	@Override
	public void processQuery(String dbName, String dbPort, String dbUser, String dbPassword, String host,
			Map<String, String> data) throws Throwable {
		Connection con = createConnection(dbName, dbPort, dbUser, dbPassword, host);
		piiProcess(con, data);
		if(!StringUtils.isEmpty(data.get("PIICOLUMNTOBEENCRYPT"))){
			columnToEncrypt(con, data);
		}
		if(!StringUtils.isEmpty(data.get("PIICOLUMNTOBEDECRYPT"))){
			columnToDecrypt(con, data);
		}
		con.close();
	}
	
	public Connection createConnection(String dbName, String dbPort, String dbUser, String dbPassword, String host) throws Throwable{
		if(StringUtils.isEmpty(dbPort)) dbPort="1433";
		if(StringUtils.isEmpty(dbUser)) dbUser="root";
		if(StringUtils.isEmpty(dbPassword)) dbPassword="root";
		if(StringUtils.isEmpty(host)) host="localhost";
		try{  
			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");  
			Connection con=DriverManager.getConnection(  
			"jdbc:sqlserver://"+host+":"+dbPort+";databaseName="+dbName,dbUser,dbPassword);
			return con;
			 
		}catch(Exception e){
			System.out.println(e);
			throw e.getCause();
		}
	}
	
	public void piiProcess(Connection con, Map<String, String> data) throws SQLException{
		Statement stmt=con.createStatement();
		if(!StringUtils.isEmpty(data.get("PIICOLUMNTOBENULL"))){
			String[] columns = data.get("PIICOLUMNTOBENULL").split(",");
			for(String col: columns) {
				StringBuilder schemaQuery = new StringBuilder();
	            schemaQuery.append("select IS_NULLABLE,DATA_TYPE, CHARACTER_MAXIMUM_LENGTH from information_schema.columns where table_name = '"+data.get("PIITABLE")+"' and COLUMN_NAME='"+col+"'");
	            ResultSet schemaResult = stmt.executeQuery(schemaQuery.toString());
	            String dattype = "";
	            String column = "";
	            while (schemaResult.next()) {
	            	column = (String) schemaResult.getObject(1);
	            	dattype = (String) schemaResult.getObject(2);
	            	if(!Objects.isNull(schemaResult.getObject(3)) && !String.valueOf( schemaResult.getObject(3)).equalsIgnoreCase("NULL")){
	            		dattype+="("+String.valueOf( schemaResult.getObject(3))+")";
	            	}
	            }
	            
	            if(!column.equalsIgnoreCase("") && !column.equalsIgnoreCase("YES")) {
	            	StringBuilder alterQuery = new StringBuilder();
	                alterQuery.append("ALTER TABLE "+data.get("PIITABLE")+" ALTER COLUMN "+col+" "+dattype+" NULL");
	                stmt.execute(alterQuery.toString());
	            }
			}
			StringBuilder updateQuery = new StringBuilder();
			updateQuery.append("UPDATE "+data.get("PIITABLE")+" set ");
			
			for(String column : columns) {
				updateQuery.append(column + "= NULL , ");
			}
			String query = updateQuery.substring(0, updateQuery.length()-2);
			System.out.println(query);
			int rs=stmt.executeUpdate(query);
		}
	}
	
	private void columnToEncrypt(Connection con, Map<String, String> data) throws Exception {
		Statement stmt=con.createStatement();
		String[] columns = data.get("PIICOLUMNTOBEENCRYPT").split(",");
		for(String col : columns){
			StringBuilder selectQuery = new StringBuilder();
			selectQuery.append("select id,"+col+" from "+data.get("PIITABLE"));
			ResultSet result = stmt.executeQuery(selectQuery.toString());
			JSONArray resultJson = convertToJSON(result);
			
			resultJson.forEach(element->{
				if(((JSONObject)element).getBoolean("isColumnAvail")){
					String plainValue = ((JSONObject)element).getString(col);
					if(plainValue!=null){
						String encryptedValue = null;
						try {
							encryptedValue = aes.encrypt(plainValue);
						} catch (Exception e) {
							throw new RuntimeException(e.getCause());
						}
						if(encryptedValue!=null){
							String updateQuery = "update "+data.get("PIITABLE")+" set "+col+ "='"+encryptedValue+"'"+" where id="+((JSONObject)element).getNumber("id");
							System.out.println("updateStrinf:::"+updateQuery);
							try {
								stmt.executeUpdate(updateQuery);
							} catch (SQLException e) {
								throw new RuntimeException(e.getCause());
							}
						}
					}
				}
			});
		}
	}
	
	public void columnToDecrypt(Connection con, Map<String, String> data) throws Exception{
		Statement stmt=con.createStatement();
		String[] columns = data.get("PIICOLUMNTOBEDECRYPT").split(",");
		for(String col : columns){
			StringBuilder selectQuery = new StringBuilder();
			selectQuery.append("select id,"+col+" from "+data.get("PIITABLE"));
			ResultSet result = stmt.executeQuery(selectQuery.toString());
			JSONArray resultJson = convertToJSON(result);
			
			resultJson.forEach(element->{
				if(((JSONObject)element).getBoolean("isColumnAvail")){
					String plainValue = ((JSONObject)element).getString(col);
					if(plainValue!=null){
						String encryptedValue = null;
						try {
							encryptedValue = aes.decrypt(plainValue);
						} catch (Exception e) {
							throw new RuntimeException("Value not encrypted");
						}
						if(encryptedValue!=null){
							String updateQuery = "update "+data.get("PIITABLE")+" set "+col+ "='"+encryptedValue+"'"+" where id="+((JSONObject)element).getNumber("id");
							System.out.println("updateStrinf:::"+updateQuery);
							try {
								stmt.executeUpdate(updateQuery);
							} catch (SQLException e) {
								throw new RuntimeException(e.getCause());
							}
						}
					}
				}
			});
		}
				  
	}
	
	public JSONArray convertToJSON(ResultSet resultSet)
            throws Exception {
        JSONArray jsonArray = new JSONArray();
        while (resultSet.next()) {
            int total_rows = resultSet.getMetaData().getColumnCount();
            JSONObject obj = new JSONObject();
            for (int i = 0; i < total_rows; i++) {
                obj.put(resultSet.getMetaData().getColumnLabel(i + 1), resultSet.getObject(i + 1));
            }
            obj.put("isColumnAvail", (obj.length()==1 ? false : true));
            jsonArray.put(obj);
        }
        return jsonArray;
    }
	
}
