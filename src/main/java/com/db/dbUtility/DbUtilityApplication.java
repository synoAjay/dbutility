package com.db.dbUtility;

import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.fasterxml.jackson.databind.ObjectMapper;

@SpringBootApplication
public class DbUtilityApplication implements CommandLineRunner{

	@Qualifier("mySqlDatabaseUtility")
	@Autowired
	private MySqlDatabaseUtility mySqlDatabaseUtility;
	
	@Qualifier("msSqlDatabaseUtility")
	@Autowired
	private MsSqlDatabaseUtility msSqlDatabaseUtility;
	
	
	public static void main(String[] args) {
		SpringApplication.run(DbUtilityApplication.class, args);
	}
	
	@Override
	public void run(String... args) throws Exception {
		ObjectMapper mapper = new ObjectMapper();

		if(args.length == 1) {
			Map<String, String> map = mapper.readValue(args[0], Map.class);
			DatabaseUtility databaseUtility = null;
			validation(map);
			
//			switch(map.get("TYPE")){
//				
//				case "MYSQL" : databaseUtility = mySqlDatabaseUtility;
//				case "MSSQL" : databaseUtility = msSqlDatabaseUtility;
//				default : System.out.println("no Type.");
//			
//			}
			
			if(map.get("TYPE") != null && !map.get("TYPE").equalsIgnoreCase("") && map.get("TYPE").equalsIgnoreCase("MYSQL")) {
				databaseUtility = mySqlDatabaseUtility;
			}else if(map.get("TYPE") != null && !map.get("TYPE").equalsIgnoreCase("") && map.get("TYPE").equalsIgnoreCase("MSSQL")) {
				databaseUtility = msSqlDatabaseUtility;
			}
			
			try {
				databaseUtility.processQuery(map.get("DATABASE"), map.get("PORT"), map.get("USERNAME"), map.get("PASSWORD"), map.get("HOST"), map);
			} catch (Throwable e) {
				System.out.println("*******************************\n\n");
				System.out.println(e.getMessage()+"\n\n");
				System.out.println("*******************************");
			}
			
			
		} else {
			System.out.println("Invalid Parameter");
			System.exit(0);
		}
	}
	
	public void validation(Map<String, String> map){
		if(!map.get("TYPE").equalsIgnoreCase("MYSQL") && !map.get("TYPE").equalsIgnoreCase("MSSQL")) {
			System.out.println("Invalid Database Type");
			System.exit(0);
		}
		Pattern p = Pattern.compile("[^A-Za-z0-9]");
	    Matcher m = p.matcher(map.get("DATABASE"));
	    boolean b = m.find();
//	    if (b) {
//	    	System.out.println("Invalid Database Name");
//	    	System.exit(0);
//	    }
        if(!map.get("PORT").matches("[0-9]+") && map.get("PORT").length() <= 3) {
            System.out.println("Invalid Port Number");
            System.exit(0);
        }
        
        String zeroTo255 = "(\\d{1,2}|(0|1)\\" + "d{2}|2[0-4]\\d|25[0-5])";
        String regex = zeroTo255 + "\\." + zeroTo255 + "\\." + zeroTo255 + "\\." + zeroTo255;
        p = Pattern.compile(regex);
        m = p.matcher(map.get("HOST"));
        b = m.matches();
        if(map.get("HOST").equalsIgnoreCase("localhost")) b=true;
        if (!b) {
            System.out.println("Invalid Host Name");
            System.exit(0);
        }
	}

}

