package com.db.dbUtility;

import java.util.Map;

public interface DatabaseUtility {
	public void processQuery(String dbName, String dbPort, String dbUser, String dbPassword, String host, Map<String, String> data) throws Throwable;
}
