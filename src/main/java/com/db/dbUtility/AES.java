package com.db.dbUtility;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import java.util.Base64;

@Component
public class AES {

	@Value("${secretKey}")
	String secretKey;
	
    public String encrypt(String strClearText) throws Exception{
    	
    	if(StringUtils.isEmpty(strClearText)) return null;
    	
        String strData="";

        try {
            SecretKeySpec skeyspec=new SecretKeySpec(secretKey.getBytes(),"AES");
            Cipher cipher=Cipher.getInstance("AES");
            cipher.init(Cipher.ENCRYPT_MODE, skeyspec);
            byte[] encrypted=cipher.doFinal(strClearText.getBytes("UTF8"));
            encrypted = Base64.getEncoder().encode(encrypted);

            strData=new String(encrypted);

        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception(e);
        }
        return strData;
    }

    public String decrypt(String strEncrypted) throws Exception{
    	
    	if(StringUtils.isEmpty(strEncrypted)) return null;
    	
        String strData="";

        try {
            SecretKeySpec skeyspec=new SecretKeySpec(secretKey.getBytes(),"AES");
            Cipher cipher=Cipher.getInstance("AES");
            cipher.init(Cipher.DECRYPT_MODE, skeyspec);
            byte[] dec = Base64.getDecoder().decode(strEncrypted.getBytes());
            byte[] decrypted=cipher.doFinal(dec);
            strData=new String(decrypted,"UTF8");

        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception(e);
        }
        return strData;
    }
}
